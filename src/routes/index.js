import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import home from '@/components/home'
import me from '@/components/about-me'
import experience from '@/components/experience'
import skill from '@/components/my-skill'

export default new VueRouter({
    routes: [{
            path: "/",
            component: home
        },
        {
            path: "/about-me",
            component: me
        },
        {
            path: "/my-skill",
            component: skill
        },
        {
            path: "/experience",
            component: experience
        },

    ]
})