import Vue from 'vue'
import App from './App.vue'

import Axios from 'axios'
Axios.defaults.baseURL='http://www.querenyanshen.cn:1001'
Vue.prototype.$http = Axios
Axios.defaults.withCredentials=true;//让ajax携带cookie
Vue.prototype.$bus = new Vue()

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import router from '@/routes'

import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper, /* { default global options } */)
import 'swiper/dist/css/swiper.css'
import { Swipe, SwipeItem } from 'vant';
Vue.use(Swipe).use(SwipeItem);
import { Divider } from 'vant';
Vue.use(Divider);
import Vant from 'vant';
import 'vant/lib/index.css';
import { Toast } from 'vant';
Vue.use(Toast);

Vue.use(Vant);

Vue.config.productionTip = false



new Vue({
  render: h => h(App),
  router
}).$mount('#app')